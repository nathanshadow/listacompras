json.extract! list, :id, :description, :merchandise, :status, :person_id, :created_at, :updated_at
json.url list_url(list, format: :json)
