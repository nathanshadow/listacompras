class List < ActiveRecord::Base
  belongs_to :person
  validates_presence_of :description
end
