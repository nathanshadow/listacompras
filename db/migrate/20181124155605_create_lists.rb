class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.string :description
      t.text :merchandise
      t.boolean :status
      t.references :person, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
